$(document).ready(function() {

    function smoothScrollingTo(target) {
        $('html,body').animate({
            scrollTop:$(target).offset().top
        }, 500);
    }

	$(".menu-toggle").click(function(event) {
        $(".menu").toggleClass("d-none");
    });

    $('.custom-navbar li a[href^="#"]').click(function(event) {
        event.preventDefault();
        $('html,body').animate({
            scrollTop:$(this.hash).offset().top
        }, 500);
        return false;
    });
});